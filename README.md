Ansible-yum-cron
=========

Sets up yum-cron.

Requirements
------------

None.

Role Variables
--------------

Variables and descriptions are in defaults/main.yml

Dependencies
------------

None.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```
---
- hosts: yum_servers
  remote_user: user
  become: true

  roles:
    - { role: ansible-yum-cron, apply_updates: "yes" }
```

License
-------

BSD

Author Information
------------------

John Hooks
